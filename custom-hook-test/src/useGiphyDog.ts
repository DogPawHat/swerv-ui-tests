import { useReducer, useEffect } from "react";

import { searchGiphy } from "./giphyApi";
import { GifpyResponse } from "./giphyTypes";
import { async } from "q";

const loading = "swerve/state/LOADING";
const success = "swerve/state/SUCCESS";
const error = "swerve/state/ERROR";
const setSuccess = "swerve/action/SET_SUCCESS";
const setError = "swerve/action/SET_ERROR";

type GiphyDogSuccessState = {
  status: typeof success;
  payload: GifpyResponse;
};

type GiphyDogErrorState = {
  status: typeof error;
}

type GiphyDogLoadingState = {
  status: typeof loading;
};

type GiphyDogState = 
  GiphyDogErrorState |
  GiphyDogLoadingState |
  GiphyDogSuccessState;

type GiphyDogSetSuccessAction = {
  type: typeof setSuccess;
  payload: GifpyResponse;
};

type GiphyDogSetErrorAction = {
  type: typeof setError;
}

type GiphyDogAction = GiphyDogSetSuccessAction | GiphyDogSetErrorAction;

const initalState: GiphyDogLoadingState = {
  status: loading
};

const reducer: (
  state: GiphyDogState,
  action: GiphyDogAction
) => GiphyDogState = (state, action) => {
  const actionHandlers = {
    [setSuccess]: () => (<GiphyDogSuccessState>{
        status: success,
        payload: (<GiphyDogSetSuccessAction>action).payload
      }),
    [setError]: () => (<GiphyDogErrorState>{
        status: error
      })
  };

  if (actionHandlers.hasOwnProperty(action.type)) {
    return actionHandlers[action.type]();
  }

  return state;
};

const useGiphyDog = () => {
  const [dogState, dispatch] = useReducer(reducer, initalState);

  useEffect(() => {
    const startDogSearch = async () => 
      searchGiphy('dog').catch(_ => Promise.resolve(null));


    startDogSearch()
      .then(async (resp) => {
        if(resp && resp.meta.status === 200) {
          dispatch({
            type: setSuccess,
            payload: resp
          })
        } else {
          dispatch({
            type: setError
          })
        }
      })
    
  }, []);

  return dogState;
};

export default useGiphyDog;
