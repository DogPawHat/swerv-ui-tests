import { renderHook, act } from '@testing-library/react-hooks';

import { searchGiphy } from './giphyApi';
import useGiphyDog from './useGiphyDog';

jest.mock('./giphyApi');

describe('useGiphyDog settings ', () => {

  it('should have a successful responses', async () => {
    searchGiphy.mockResolvedValue({
      meta: {
        status: 200,
        msg: 'OK'
      },
      pagination: {
        count: 25,
        total_count: 8000
      }
    });

    const { result, waitForNextUpdate } = renderHook(() => useGiphyDog());

    expect(result.current.status).toBe('swerve/state/LOADING');

    // This will complain about not being wrapped in act()
    // but it can't be fixed until 16.9
    // And It's just a warning
    await waitForNextUpdate();

    const successState = result.current;

    expect(successState.status).toBe('swerve/state/SUCCESS');

    // Typescript refine type
    if(successState.status === 'swerve/state/SUCCESS') {
      expect(successState.payload).toBeDefined();
      expect(successState.payload.meta.status).toBe(200);
      expect(successState.payload.meta.msg).toBe('OK');
      expect(successState.payload.pagination.count).toBe(25);
      expect(successState.payload.pagination.total_count).toBe(8000);
    }
  });

  it('should have a failed response 1', async () => {
    searchGiphy.mockRejectedValue()

     const { result, waitForNextUpdate } = renderHook(() => useGiphyDog());

    expect(result.current.status).toBe('swerve/state/LOADING');

    await waitForNextUpdate();

    const successState = result.current;

    expect(successState.status).toBe('swerve/state/ERROR');

  })

  it('should have a failed response 2', async () => {
    searchGiphy.mockRejectedValue({
      meta: {
        status: 404,
        msg: 'File Not Found'
      }
    })

     const { result, waitForNextUpdate } = renderHook(() => useGiphyDog());

    expect(result.current.status).toBe('swerve/state/LOADING');

    await waitForNextUpdate();

    const successState = result.current;

    expect(successState.status).toBe('swerve/state/ERROR');

  })
})