export type GiphyImages = {
  fixed_height: {
    url: string;
  }
}

export type GifpyObject = {
  url: string;
  images: GiphyImages;
}

export type GifpyMeta = {
  msg: string;
  status: number;
}

export type GifpyPagination = {
  offset: number;
  total_count: number;
  count: number;
}


export type GifpyResponse = {
  data: GifpyObject[];
  pagination: GifpyPagination;
  meta: GifpyMeta;
}