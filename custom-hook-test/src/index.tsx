import React from "react";
import { render } from 'react-dom'

import "./styles.css";
import useGiphyDog from "./useGiphyDog";

function App() {
  const dogState = useGiphyDog();

  return (
    <div className="app">
      <h2>Render the API response within the container below.</h2>
      <pre id="response-container">
        {dogState.status === "swerve/state/SUCCESS"
          ? `
            Status: ${dogState.payload.meta.status} 
            Message: ${dogState.payload.meta.msg}
            Count: ${dogState.payload.pagination.count}
            Total Count: ${dogState.payload.pagination.total_count}
          `
          : null}
        {dogState.status === "swerve/state/LOADING" ? (
          "Loading..."
        ) : null}
        {dogState.status === "swerve/state/ERROR" ? (
          "Error"
        ) : null}
      </pre>
      {dogState.status === "swerve/state/SUCCESS"
        ? dogState.payload.data.map(obj => <img src={obj.images.fixed_height.url} />)
        : null}
    </div>
  );
}

const rootElement = document.getElementById("root");
render(<App />, rootElement);
