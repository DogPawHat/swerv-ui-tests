import { GifpyResponse } from "./giphyTypes";

const endPoint = "https://api.giphy.com/v1/gifs/search";

const apiKey = "oZsmIOhSoqmZiyy3YCIrMhc7d9Whcuh9";

export const searchGiphy = async (query: string) => {
  try {
    const url = `${endPoint}?q=${query}&api_key=${apiKey}`;
    const resp = await fetch(url);

    return <Promise<GifpyResponse>>(resp.json())
  } catch {
    
  }
};
