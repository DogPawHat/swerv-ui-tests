// N types are normilized entities, U types are unnormalized


/**
 * User type is already normalized
 */
declare type User = {
    username: string;
    name: string;
}

declare type UComment = {
    id: string,
    author: User,
    comment: string
}

declare type NComment = {
    id: string,
    author: string,
    comment: string
}

declare type UPost = {
    id: string,
    author: User,
    body: string,
    comments: UComment[]
};

declare type NPost = {
    id: string,
    author: string,
    body: string,
    comments: string[]
};

declare type GenericCollection<T> = {
    byId: {
        [key: string]: T
    },
    allIds: string[]
}


/**
 * Union type of all normolised types
 */
declare type NEntity = User | NComment | NPost

declare type NPostCollection = GenericCollection<NPost>
declare type NCommentCollection = GenericCollection<NComment>
declare type UserCollection = GenericCollection<User>

declare type EntityCollection = GenericCollection<NEntity>

/**
 * Example data set before normailzation: array of unnormalized posts
 */
declare type Before = UPost[];

/**
 * Example data set after normailzation
 */
declare type After = {
    posts: NPostCollection,
    comments: NCommentCollection,
    users: UserCollection
}