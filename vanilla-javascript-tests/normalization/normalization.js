// @ts-check
/// <reference types="node" />
/// <reference path="./normalization.d.ts" />

/**
 * @type Before
 */
const blogPostEntries = [
  {
    id: "post1",
    author: { username: "user1", name: "User 1" },
    body: "The quick, brown fox jumps over a lazy dog.",
    comments: [
      {
        id: "comment1",
        author: { username: "user2", name: "User 2" },
        comment: "I like dogs!"
      }
    ]
  },
  {
    id: "post2",
    author: { username: "user2", name: "User 2" },
    body: "It was the best of times, it was the blurst of times...",
    comments: [
      {
        id: "comment2",
        author: { username: "user3", name: "User 3" },
        comment: "Did Charles Dickens write this?"
      },
      {
        id: "comment3",
        author: { username: "user4", name: "User 4" },
        comment: "No, it was someone else I think."
      },
      {
        id: "comment4",
        author: { username: "user3", name: "User 3" },
        comment: "It's from a Tale of Two Cities"
      }
    ]
  },
  {
    id: "post3",
    author: { username: "user2", name: "User 2" },
    body: "Please comment on this post!",
    comments: []
  },
  {
    id: "post4",
    author: { username: "user5", name: "User 5" },
    body:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    comments: [
      {
        id: "comment5",
        author: { username: "user4", name: "User 4" },
        comment: "Wow, I didn't know you spoke Italian!"
      },
      {
        id: "comment6",
        author: { username: "user5", name: "User 5" },
        comment: "It isn't Italian."
      },
      {
        id: "comment7",
        author: { username: "user4", name: "User 4" },
        comment: "Oh, is it Greek?"
      },
      {
        id: "comment8",
        author: { username: "user4", name: "User 4" },
        comment: "Oh, hang on, it's Latin!"
      }
    ]
  },
  {
    id: "post5",
    author: { username: "user4", name: "User 4" },
    body: "What is your favourite genre of music?",
    comments: [
      {
        id: "comment9",
        author: { username: "user2", name: "User 2" },
        comment: "Mine is rock!"
      },
      {
        id: "comment10",
        author: { username: "user3", name: "User 3" },
        comment: "Rock is a terrible genre. The best genre is country."
      },
      {
        id: "comment11",
        author: { username: "user4", name: "User 4" },
        comment: "I think country can be nice, but I prefer blues overall."
      },
      {
        id: "comment12",
        author: { username: "user6", name: "User 6" },
        comment: "Jazz is objectively the best genre."
      },
      {
        id: "comment13",
        author: { username: "user5", name: "User 5" },
        comment: "I like all genres of music!"
      }
    ]
  }
];

/**
 * @template T
 * @param {GenericCollection<T>} acc
 * @param {GenericCollection<T>} newObject
 * @returns Collection
 */
const reduceToUnqueId = (acc, newObject) => {
  const newAllIds = [...acc.allIds, ...newObject.allIds].filter(
    (v, idx, self) => self.indexOf(v) === idx
  );

  return {
    byId: {
      ...acc.byId,
      ...newObject.byId
    },
    allIds: newAllIds
  };
};

/**
 * @param {UComment} comment
 * @return {NCommentCollection}
 */
const getCommentCollection = comment => ({
  byId: {
    [comment.id]: {
      id: comment.id,
      author: comment.author.username,
      comment: comment.comment
    }
  },
  allIds: [comment.id]
});

/**
 * @param {User} user
 * @return {UserCollection}
 */
const getUserCollection = user => ({
  byId: {
    [user.username]: {
      username: user.username,
      name: user.name
    }
  },
  allIds: [user.username]
});

/**
 * @param {UPost} post
 * @return {NPostCollection}
 */
const getPostCollection = post => ({
  byId: {
    [post.id]: {
      id: post.id,
      author: post.author.username,
      body: post.body,
      comments: post.comments.map(comment => comment.id)
    }
  },
  allIds: [post.id]
});

/**
 * @param {UComment} comment
 * @return {Pick<After, 'comments' | 'users'>;}
 */
const processComments = comment => ({
  comments: getCommentCollection(comment),
  users: getUserCollection(comment.author)
});

/**
 * @param {UPost} post
 * @return {After}
 */
const processPosts = post => ({
  posts: getPostCollection(post),
  ...post.comments.map(processComments).reduce(
    (acc, comment) => ({
      comments: reduceToUnqueId(acc.comments, comment.comments),
      users: reduceToUnqueId(acc.users, comment.users)
    }),
    {
      comments: {
        byId: {},
        allIds: []
      },
      users: getUserCollection(post.author)
    }
  )
});

/**
 *
 * @param {Before} posts
 * @returns {After}
 */
const nomalizeBlogPostEntries = posts =>
  posts.map(processPosts).reduce(
    (acc, post) => ({
      posts: reduceToUnqueId(acc.posts, post.posts),
      comments: reduceToUnqueId(acc.comments, post.comments),
      users: reduceToUnqueId(acc.users, post.users)
    }),
    {
      posts: {
        byId: {},
        allIds: []
      },
      comments: {
        byId: {},
        allIds: []
      },
      users: {
        byId: {},
        allIds: []
      }
    }
  );

/**
 * @type {After}
 * 
 */
const normalized = nomalizeBlogPostEntries(blogPostEntries);

const testStructure = () => {
  // Test each property
  if(
    normalized == undefined &&
    normalized.posts == undefined &&
    normalized.comments == undefined &&
    normalized.users == undefined
  ) {
    return false;
  }

  /**
   * @type {EntityCollection[]}
   */
  const collections = Object.values(normalized);

  let collectionCorrect = true;
  collections.forEach(collection => {
    if(collection.allIds == undefined || collection.allIds.length === 0) {
      collectionCorrect = false;
    }
    if(collection.byId == undefined || Object.keys(collection.byId).length === 0) {
      collectionCorrect = false;
    }
  })

  return collectionCorrect;
}

const testMsg = testStructure() ? "OK" : "FAIL";

console.log("Test: " + testMsg);

console.log(require('util').inspect(normalized, {showHidden: false, depth: null}));

