declare type Entry = {
    _id: string,
    name: string,
    description: string,
    startDate: string,
    channel: string
}

declare type CountState = {
    topDates: string[],
    topCount: number,
    currentDate: string,
    currentCount: number
}