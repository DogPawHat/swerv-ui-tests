// @ts-check
/// <reference types="node" />
/// <reference path="./sort.d.ts" />

const fs = require("fs");

/** @type {Entry[]} */
const contents = JSON.parse(
  fs.readFileSync("./dataset.json", { encoding: "UTF-8" })
);

/**
 *
 * @param {string} base
 */
const dateString = base => `${base}T00:00:000Z`;

/**
 *
 * @param {string} base
 */
const getDate = base => new Date(base);

/**
 *
 * @param {Date} before
 * @param {Date} after
 */
const dateCompare = (before, after) => {
  if (before.valueOf() < after.valueOf()) return -1;

  if (before.valueOf() > after.valueOf()) return 1;

  return 0;
};

/**
 *
 * @param {Entry} before
 * @param {Entry} after
 */
const stateDateCompare = (before, after) =>
  dateCompare(getDate(before.startDate), getDate(after.startDate));

const sortedJsonContents = contents.sort(stateDateCompare);

/**
 *
 * @param {string} channel
 */
const getChannelGroup = channel =>
  sortedJsonContents
    .filter(entry => channel === entry.channel)
    .sort(stateDateCompare);

/**
 *
 * @param {CountState} state
 * @param {Entry} entry
 * @return {CountState}
 */
const counterReducer = (state, entry) => {
  let newState;
  if (state.currentDate === "") {
    newState = {
      currentDate: entry.startDate,
      currentCount: 1,
      topCount: 1,
      topDates: [entry.startDate]
    };
  } else if (state.currentDate === entry.startDate) {
    const newCount = state.currentCount + 1;

    if (newCount === state.topCount) {
      newState = {
        ...state,
        currentCount: newCount,
        topDates: [...state.topDates, entry.startDate]
      };
    } else if (newCount > state.topCount) {
      newState = {
        ...state,
        topCount: newCount,
        currentCount: newCount,
        topDates: [entry.startDate]
      };
    } else if (newCount < state.topCount) {
      newState = {
        ...state,
        currentCount: newCount
      };
    }
  } else {
    newState = {
      ...state,
      currentCount: 1,
      currentDate: entry.startDate
    };
  }

  return newState;
};

/**
 *
 * @param {string} channel
 */
const getPopularDatesForChannel = channel =>
  getChannelGroup(channel).reduce(counterReducer, {
    topDates: [],
    currentCount: 0,
    topCount: 0,
    currentDate: ""
  }).topDates;

const result = {
  InAppMessage: getPopularDatesForChannel("InAppMessage"),
  PushNotification: getPopularDatesForChannel("PushNotification"),
  Conversations: getPopularDatesForChannel("Conversations"),
  Geo: getPopularDatesForChannel("Geo"),
  Email: getPopularDatesForChannel("Email")
};

const testOutput = {
  PushNotification: ["2019-01-02", "2019-01-03"],
  InAppMessage: ["2019-01-04"],
  Conversations: ["2019-01-01"],
  Email: ["2019-01-02"],
  Geo: ["2019-01-02"]
};

/**
 * test via deep array comparison
 */
if (
  Object.keys(testOutput).reduce(
    (_, key) =>
      result[key] &&
      result[key].reduce(
        (_, innerValue, innerKey) => typeof innerValue === 'string' &&
          innerValue === testOutput[key][innerKey],
        true
      ),
    true
  )
) {
  console.log("Test: OK");
} else {
  console.log("Test: FAIL");
}

console.log(result);
