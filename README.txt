Prerequesites:
 - node 10.15
 - yarn 1.17

To view the source files, I recommend using Visual Studio Code or other TS-enabled editor

How to run projects;

custom-hook-test
 - cd custom-instructions
 - yarn
 - To run app: yarn start
 - To test: yarn test

vanilla-javascript-tests > normalization
 - cd vanilla-javascript-test/normalization
 - node normalization.js

vanilla-javascript-tests > sort
 - cd vanilla-javascript-test/sort
 - node sort.js

For each vanilla-javascript-tests file, you should see a "Test: OK" output indicated the test succeeded (or a "Test: FAIL" if it fails) 

custom-hook-test has been written in Typescript for type safety, while vanilla-javascript-tests have type delaration files and JSDoc Typescript annotations

Having typesafe code elimatates a number of bugs related to undefined values and incorrect properties and can be considered a low-level form of testing at the build level.
